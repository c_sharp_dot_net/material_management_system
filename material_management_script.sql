-- #creation_of_databbase
CREATE DATABASE material_management;

-- #Use_databse
USE material_management;

-- #Create_tables_for_login (2)
CREATE TABLE Tblusermaster(
userid INT CONSTRAINT[PK_Tblusermaster] PRIMARY KEY IDENTITY,
username VARCHAR(20) CONSTRAINT[UK_Tblusermaster] UNIQUE,
password NVARCHAR(100),
con_password NVARCHAR(100));

CREATE TABLE Tbluserdetail(
userdetailid INT CONSTRAINT[PK_Tbluserdetail] PRIMARY KEY IDENTITY,
userid INT CONSTRAINT[FK_Tbluserdetail] FOREIGN KEY REFERENCES Tblusermaster,
fname VARCHAR(20),
mname VARCHAR(20),
lname VARCHAR(20),
fullname AS fname+' '+mname+' '+lname,
emailid NVARCHAR(15),
mobileno NVARCHAR(15),
created_date DATETIME,
created_by NVARCHAR(20),
modified_date DATETIME,
modified_by NVARCHAR(20));

-- #creating_PROCEDURE_for_new_USER
CREATE PROCEDURE p_man_udetail
@userid INT =NULL,
@username VARCHAR(20)=NULL,
@password NVARCHAR(100)=NULL,
@con_password NVARCHAR(100)=NULL,
@fname VARCHAR(20)=NULL,
@mname VARCHAR(20)=NULL,
@lname VARCHAR(20)=NULL,
@emailid NVARCHAR(15)=NULL,
@mobileno NVARCHAR(15)=NULL,
@created_by NVARCHAR(20)=NUll,
@modified_by NVARCHAR(20)=NULL,
@Error_msg VARCHAR(500)=NULL OUTPUT AS 
BEGIN TRAN 
 BEGIN TRY
  if @userid IS NULL
  BEGIN
   INSERT INTO Tblusermaster (username,password,con_password)
   VALUES(@username,ENCRYPTBYPASSPHRASE('Raaven',@password),ENCRYPTBYPASSPHRASE('Raaven',@con_password))
   SET @userid=@@IDENTITY
   INSERT INTO Tbluserdetail (userid,fname,mname,lname,emailid,mobileno,created_by,created_date)
   VALUES(@userid,@fname,@mname,@lname,@emailid,@mobileno,@created_by,GETDATE())
  END
  ELSE
  BEGIN
   UPDATE Tblusermaster SET username=@username, password=ENCRYPTBYPASSPHRASE('Raaven',@password),
   con_password=ENCRYPTBYPASSPHRASE('Raaven',@con_password) WHERE userid=@userid

   UPDATE Tbluserdetail SET fname=@fname, mname=@mname,  lname=@lname, 
   emailid=@emailid, mobileno=@mobileno,  modified_by=@modified_by,
   modified_date=GETDATE() WHERE userid=@userid
  END
 END TRY
 BEGIN CATCH
ROLLBACK TRAN
  SET @Error_msg=ERROR_LINE()
  RETURN -1
 END CATCH
COMMIT TRAN
 RETURN 1

 -- #creating_view_for_new_user
 CREATE VIEW v_man_udetail AS
 SELECT Tblusermaster.userid, Tblusermaster.username,
 CONVERT(NVARCHAR(100),
 DECRYPTBYPASSPHRASE('Raaven',Tblusermaster.password)) 
 AS password,CONVERT(NVARCHAR(100),
 DECRYPTBYPASSPHRASE('Raaven',Tblusermaster.con_password))
 AS con_password,Tbluserdetail.fname,Tbluserdetail.mname,
 Tbluserdetail.lname,Tbluserdetail.fullname,
 Tbluserdetail.mobileno,
 Tbluserdetail.emailid,Tbluserdetail.created_date,
 Tbluserdetail.created_by,Tbluserdetail.modified_date,
 Tbluserdetail.modified_by
 FROM Tblusermaster JOIN Tbluserdetail 
 ON Tblusermaster.userid = Tbluserdetail.userid

 -- #creating table for configmaster
 CREATE TABLE Tblconfigmaster(
 configid INT CONSTRAINT[PK_Tblconfigmaster] PRIMARY KEY IDENTITY,
 shopname VARCHAR(100) CONSTRAINT[UK_Tblconfigmaster] UNIQUE,
 propwriter VARCHAR(63),
 adress NVARCHAR(100),
 mobileno NVARCHAR(15),
 contactno NVARCHAR(15),
 emailid NVARCHAR(15),
 created_date DATETIME,
 created_by NVARCHAR(20),
 modified_date DATETIME,
 modified_by NVARCHAR(20));

 -- #createing Procedure For configmaster
 CREATE PROCEDURE p_configmaster
 @configid INT=NULL,
 @shopname VARCHAR(100)=NULL,
 @propwriter VARCHAR(63)=NULL,
 @adress NVARCHAR(100)=NULL,
 @mobileno NVARCHAR(15)=NULL,
 @contactno NVARCHAR(15)=NULL,
 @emailid NVARCHAR(15)=NULL,
 @created_by NVARCHAR(20)=NULL,
 @modified_by NVARCHAR(20)=NULL,
 @Error_msg VARCHAR(500)=NULL OUTPUT AS 
 BEGIN TRAN
  BEGIN TRY
   IF @configid IS NULL
   BEGIN
    INSERT INTO Tblconfigmaster (shopname,propwriter,adress,mobileno,
	contactno,emailid,created_by,created_date) 
	VALUES(@shopname,@propwriter,@adress,@mobileno,
	@contactno,@emailid,@created_by,GETDATE())
   END
   ELSE
   BEGIN
    UPDATE Tblconfigmaster SET shopname=@shopname, propwriter=@propwriter,
	adress=@adress, mobileno=@mobileno, contactno=@contactno, emailid=@emailid,
	modified_by=@modified_by, modified_date=GETDATE() WHERE configid=@configid
   END
  END TRY
  BEGIN CATCH
ROLLBACK TRAN
  SET @Error_msg=ERROR_LINE()
  RETURN -1
 END CATCH
COMMIT TRAN
 RETURN 1