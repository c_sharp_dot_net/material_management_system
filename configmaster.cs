﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.SqlTypes;

namespace material_management_sysem
{
    public partial class configmaster : Form
    {
        SqlConnection con = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["Raaven"].ToString());
        public configmaster()
        {
            InitializeComponent();
        }

        private void configmaster_Load(object sender, EventArgs e)
        {

        }

        // #validation
        private Boolean valid()
        {
            if (txtsname.Text == "")
            {
                MessageBox.Show("Please Enter Shopname");
                txtsname.Focus();
                return false;
            }
            if (txtpropwriter.Text == "")
            {
                MessageBox.Show("Please Enter Name Of Propwriter");
                txtpropwriter.Focus();
                return false;
            }
            if (txtcono.Text == "")
            {
                MessageBox.Show("Please Enter Contacr Number");
                txtcono.Focus();
                return false;
            }
            if (txteid.Text == "")
            {
                MessageBox.Show("Please Enter Email ID");
            }
            if (txtadress.Text == "")
            {
                MessageBox.Show("Please Enter Adress Of Propwriter");
                txtadress.Focus();
                return false;
            }
            return true;
        }

        // #Logic : Save_button
        private void btnsave_Click(object sender, EventArgs e)
        {
            if (valid())
            {
                try
                {
                    // #Logic : For passing data from txtbox to the database via procedure
                    SqlCommand cmd = new SqlCommand();
                    if (txtcid.Text == "")
                    {
                        cmd.Parameters.AddWithValue("@configid", null);
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@configid", txtcid.Text);
                    }
                    cmd.Parameters.AddWithValue("@shopname", txtsname.Text);
                    cmd.Parameters.AddWithValue("@propwriter", txtpropwriter.Text);
                    cmd.Parameters.AddWithValue("@mobileno", txtmono.Text);
                    cmd.Parameters.AddWithValue("@contactno", txtcono.Text);
                    cmd.Parameters.AddWithValue("@emailid", txteid.Text);
                    cmd.Parameters.AddWithValue("@Adress", txtadress.Text);
                    cmd.Parameters.AddWithValue("@created_by", Login.Username);
                    cmd.Parameters.AddWithValue("@modified_by", Login.Username);

                    // #Logic : getting error msg from database if it is occurs
                    SqlParameter ermsg = cmd.Parameters.Add("@Error_msg", SqlDbType.VarChar, 500);
                    ermsg.Direction = ParameterDirection.Output;

                    // #Logic : getting return value when procedure is executed
                    SqlParameter rvalue = cmd.Parameters.Add("@ret", SqlDbType.Int);
                    rvalue.Direction = ParameterDirection.ReturnValue;

                    // #Logic : Execution of procedure
                    cmd.CommandText = "p_configmaster";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = con;
                    con.Open();
                    cmd.ExecuteNonQuery();

                    int ret = 0;
                    ret = (int)rvalue.Value;

                    // ##Logic : for success
                    if (ret > 0)
                    {
                        MessageBox.Show("Data Saved Sucessfully");
                        // ### Loogic : clearing data prescent in the txtbox
                        txtsname.Clear();
                        txtpropwriter.Clear();
                        txtmono.Clear();
                        if (cbmono.CheckState == CheckState.Checked)
                        {
                            cbmono.CheckState = CheckState.Unchecked;
                        }
                        txtcono.Clear();
                        txteid.Clear();
                        txtadress.Clear();
                        txtssname.Clear();
                        txtcid.Clear();
                        txtcdate.Clear();
                        txtcby.Clear();
                        txtmdate.Clear();
                        txtmby.Clear();
                    }
                    else
                    {
                        // ##Logic : printing Error while executiong procedure if it occurs
                        string str = (string)ermsg.Value;
                        MessageBox.Show(str);
                    }
                }
                catch (Exception ex)
                {
                    // ##Logic : printing error while executing try  block
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    con.Close();
                }
            }
        }


        // #Logic : contact_no and and mobile_no are same
        private void cbmono_CheckedChanged(object sender, EventArgs e)
        {
            if (cbmono.Checked)
            {
                txtcono.Text = txtmono.Text;
            }
            else
            {
                txtcono.Clear();
            }
        }

        // #search button 
        // ##Logic : Displaying data inside list box
        private void txtssname_TextChanged(object sender, EventArgs e)
        {
            if (txtssname.Text != "")
            {
                SqlDataAdapter adapter = new SqlDataAdapter("select shopname from Tblconfigmaster where shopname like '" + txtssname.Text + "%'", con);
                DataSet ds = new DataSet();
                adapter.Fill(ds);
                lbsname.Items.Clear();

                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        lbsname.Visible = true;
                        lbsname.Items.Add(dr[0]);
                    }
                }
                else
                {
                    lbsname.Visible = false;
                }
            }
            else
            {
                lbsname.Visible = false;
            }
        }

        //##Logic : Taking  focus on list box
        private void txtssname_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Down)
            {
                lbsname.Focus();
            }
        }

        //##Logic :  Searching User Data
        private void lbsname_KeyPress(object sender, KeyPressEventArgs e)
        {
            //###Logic : Inserting shopname from listbox to txtbox for searching
            if (e.KeyChar == 13)
            {
                txtssname.Text = lbsname.SelectedItem.ToString();
                lbsname.Visible = false;
            }

            //###Logic : Searching and filling user data by shopname
            SqlDataAdapter adapter = new SqlDataAdapter("select * from Tblconfigmaster where shopname='" + txtssname.Text + "'", con);
            DataSet ds = new DataSet();
            adapter.Fill(ds);

            if (ds.Tables[0].Rows.Count > 0)
            {
                txtcid.Text = ds.Tables[0].Rows[0][0].ToString();
                txtsname.Text = ds.Tables[0].Rows[0][1].ToString();
                txtpropwriter.Text = ds.Tables[0].Rows[0][2].ToString();
                txtadress.Text = ds.Tables[0].Rows[0][3].ToString();
                txtmono.Text = ds.Tables[0].Rows[0][4].ToString();
                txtcono.Text = ds.Tables[0].Rows[0][5].ToString();
                txteid.Text = ds.Tables[0].Rows[0][6].ToString();
                txtcdate.Text = ds.Tables[0].Rows[0][7].ToString();
                txtcby.Text = ds.Tables[0].Rows[0][8].ToString();
                txtmdate.Text = ds.Tables[0].Rows[0][9].ToString();
                txtmby.Text = ds.Tables[0].Rows[0][10].ToString();

                if (txtcono.Text == txtmono.Text)
                {
                    cbmono.CheckState = CheckState.Checked;
                }
            }
        }

        // # Logic : Clear Button
        private void btnclear_Click(object sender, EventArgs e)
        {
            txtsname.Clear();
            txtpropwriter.Clear();
            txtmono.Clear();
            if (cbmono.CheckState == CheckState.Checked)
            {
                cbmono.CheckState = CheckState.Unchecked;
            }
            txtcono.Clear();
            txteid.Clear();
            txtadress.Clear();
            txtssname.Clear();
            txtcid.Clear();
            txtcdate.Clear();
            txtcby.Clear();
            txtmdate.Clear();
            txtmby.Clear();
        }

        // # Logic : exit Button
        private void btnexit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
