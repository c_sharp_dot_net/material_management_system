﻿
namespace material_management_sysem
{
    partial class password_man
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtopassword = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txtflname = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtuid = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.btnexit = new System.Windows.Forms.Button();
            this.btnclear = new System.Windows.Forms.Button();
            this.btnsave = new System.Windows.Forms.Button();
            this.lbuname = new System.Windows.Forms.ListBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.txtmby = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtcby = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtmdate = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtcdate = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtsuname = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txteid = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtmono = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtlname = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtmname = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtfname = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtcpassword = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtnpassword = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtuname = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // txtopassword
            // 
            this.txtopassword.Font = new System.Drawing.Font("Wingdings", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.txtopassword.Location = new System.Drawing.Point(147, 172);
            this.txtopassword.Name = "txtopassword";
            this.txtopassword.PasswordChar = 'l';
            this.txtopassword.Size = new System.Drawing.Size(170, 22);
            this.txtopassword.TabIndex = 2;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(26, 172);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(95, 17);
            this.label20.TabIndex = 158;
            this.label20.Text = "Old Password";
            // 
            // txtflname
            // 
            this.txtflname.Enabled = false;
            this.txtflname.Location = new System.Drawing.Point(504, 482);
            this.txtflname.Name = "txtflname";
            this.txtflname.Size = new System.Drawing.Size(178, 22);
            this.txtflname.TabIndex = 157;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(403, 482);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(71, 17);
            this.label19.TabIndex = 156;
            this.label19.Text = "Full Name";
            // 
            // txtuid
            // 
            this.txtuid.Enabled = false;
            this.txtuid.Location = new System.Drawing.Point(507, 263);
            this.txtuid.Name = "txtuid";
            this.txtuid.Size = new System.Drawing.Size(178, 22);
            this.txtuid.TabIndex = 155;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(406, 263);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(55, 17);
            this.label18.TabIndex = 154;
            this.label18.Text = "User ID";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(515, 9);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(200, 22);
            this.dateTimePicker1.TabIndex = 153;
            // 
            // btnexit
            // 
            this.btnexit.Location = new System.Drawing.Point(503, 537);
            this.btnexit.Name = "btnexit";
            this.btnexit.Size = new System.Drawing.Size(75, 36);
            this.btnexit.TabIndex = 152;
            this.btnexit.Text = "Exit";
            this.btnexit.UseVisualStyleBackColor = true;
            this.btnexit.Click += new System.EventHandler(this.btnexit_Click);
            // 
            // btnclear
            // 
            this.btnclear.Location = new System.Drawing.Point(409, 537);
            this.btnclear.Name = "btnclear";
            this.btnclear.Size = new System.Drawing.Size(75, 36);
            this.btnclear.TabIndex = 151;
            this.btnclear.Text = "Clear";
            this.btnclear.UseVisualStyleBackColor = true;
            this.btnclear.Click += new System.EventHandler(this.btnclear_Click);
            // 
            // btnsave
            // 
            this.btnsave.Location = new System.Drawing.Point(29, 537);
            this.btnsave.Name = "btnsave";
            this.btnsave.Size = new System.Drawing.Size(75, 36);
            this.btnsave.TabIndex = 150;
            this.btnsave.Text = "Save";
            this.btnsave.UseVisualStyleBackColor = true;
            this.btnsave.Click += new System.EventHandler(this.btnsave_Click);
            // 
            // lbuname
            // 
            this.lbuname.FormattingEnabled = true;
            this.lbuname.ItemHeight = 16;
            this.lbuname.Location = new System.Drawing.Point(504, 148);
            this.lbuname.Name = "lbuname";
            this.lbuname.Size = new System.Drawing.Size(178, 52);
            this.lbuname.TabIndex = 149;
            this.lbuname.Visible = false;
            this.lbuname.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.lbuname_KeyPress);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(382, 219);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(168, 17);
            this.label17.TabIndex = 148;
            this.label17.Text = "For Technical Information";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(382, 98);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(97, 17);
            this.label16.TabIndex = 147;
            this.label16.Text = "For Searching";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(14, 98);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(177, 17);
            this.label15.TabIndex = 146;
            this.label15.Text = "For Insertion or Updatation";
            // 
            // txtmby
            // 
            this.txtmby.Enabled = false;
            this.txtmby.Location = new System.Drawing.Point(504, 442);
            this.txtmby.Name = "txtmby";
            this.txtmby.Size = new System.Drawing.Size(178, 22);
            this.txtmby.TabIndex = 145;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(403, 442);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(81, 17);
            this.label14.TabIndex = 144;
            this.label14.Text = "Modified By";
            // 
            // txtcby
            // 
            this.txtcby.Enabled = false;
            this.txtcby.Location = new System.Drawing.Point(504, 349);
            this.txtcby.Name = "txtcby";
            this.txtcby.Size = new System.Drawing.Size(178, 22);
            this.txtcby.TabIndex = 143;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(403, 352);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(78, 17);
            this.label13.TabIndex = 142;
            this.label13.Text = "Created By";
            // 
            // txtmdate
            // 
            this.txtmdate.Enabled = false;
            this.txtmdate.Location = new System.Drawing.Point(504, 400);
            this.txtmdate.Name = "txtmdate";
            this.txtmdate.Size = new System.Drawing.Size(178, 22);
            this.txtmdate.TabIndex = 141;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(403, 400);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(95, 17);
            this.label12.TabIndex = 140;
            this.label12.Text = "Modified Date";
            // 
            // txtcdate
            // 
            this.txtcdate.Enabled = false;
            this.txtcdate.Location = new System.Drawing.Point(504, 306);
            this.txtcdate.Name = "txtcdate";
            this.txtcdate.Size = new System.Drawing.Size(178, 22);
            this.txtcdate.TabIndex = 139;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(403, 306);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(92, 17);
            this.label11.TabIndex = 138;
            this.label11.Text = "Created Date";
            // 
            // txtsuname
            // 
            this.txtsuname.Location = new System.Drawing.Point(504, 128);
            this.txtsuname.Name = "txtsuname";
            this.txtsuname.Size = new System.Drawing.Size(178, 22);
            this.txtsuname.TabIndex = 1;
            this.txtsuname.TextChanged += new System.EventHandler(this.txtsuname_TextChanged);
            this.txtsuname.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtsuname_KeyDown);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(403, 128);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(73, 17);
            this.label10.TabIndex = 136;
            this.label10.Text = "Username";
            // 
            // txteid
            // 
            this.txteid.Enabled = false;
            this.txteid.Location = new System.Drawing.Point(147, 495);
            this.txteid.Name = "txteid";
            this.txteid.Size = new System.Drawing.Size(170, 22);
            this.txteid.TabIndex = 135;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(26, 495);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(59, 17);
            this.label8.TabIndex = 134;
            this.label8.Text = "Email ID";
            // 
            // txtmono
            // 
            this.txtmono.Enabled = false;
            this.txtmono.Location = new System.Drawing.Point(147, 447);
            this.txtmono.Name = "txtmono";
            this.txtmono.Size = new System.Drawing.Size(170, 22);
            this.txtmono.TabIndex = 133;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(26, 447);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(78, 17);
            this.label9.TabIndex = 132;
            this.label9.Text = "Mobile NO.";
            // 
            // txtlname
            // 
            this.txtlname.Enabled = false;
            this.txtlname.Location = new System.Drawing.Point(147, 402);
            this.txtlname.Name = "txtlname";
            this.txtlname.Size = new System.Drawing.Size(170, 22);
            this.txtlname.TabIndex = 131;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(26, 402);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(76, 17);
            this.label5.TabIndex = 130;
            this.label5.Text = "Last Name";
            // 
            // txtmname
            // 
            this.txtmname.Enabled = false;
            this.txtmname.Location = new System.Drawing.Point(147, 354);
            this.txtmname.Name = "txtmname";
            this.txtmname.Size = new System.Drawing.Size(170, 22);
            this.txtmname.TabIndex = 129;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(26, 354);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(90, 17);
            this.label6.TabIndex = 128;
            this.label6.Text = "Middle Name";
            // 
            // txtfname
            // 
            this.txtfname.Enabled = false;
            this.txtfname.Location = new System.Drawing.Point(147, 311);
            this.txtfname.Name = "txtfname";
            this.txtfname.Size = new System.Drawing.Size(170, 22);
            this.txtfname.TabIndex = 127;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(26, 311);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(76, 17);
            this.label7.TabIndex = 126;
            this.label7.Text = "First Name";
            // 
            // txtcpassword
            // 
            this.txtcpassword.Font = new System.Drawing.Font("Wingdings", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.txtcpassword.Location = new System.Drawing.Point(147, 267);
            this.txtcpassword.Name = "txtcpassword";
            this.txtcpassword.PasswordChar = 'l';
            this.txtcpassword.Size = new System.Drawing.Size(170, 22);
            this.txtcpassword.TabIndex = 125;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(26, 267);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(121, 17);
            this.label4.TabIndex = 124;
            this.label4.Text = "Confirm Password";
            // 
            // txtnpassword
            // 
            this.txtnpassword.Font = new System.Drawing.Font("Wingdings", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.txtnpassword.Location = new System.Drawing.Point(147, 219);
            this.txtnpassword.Name = "txtnpassword";
            this.txtnpassword.PasswordChar = 'l';
            this.txtnpassword.Size = new System.Drawing.Size(170, 22);
            this.txtnpassword.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(26, 219);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(100, 17);
            this.label3.TabIndex = 122;
            this.label3.Text = "New Password";
            // 
            // txtuname
            // 
            this.txtuname.Enabled = false;
            this.txtuname.Location = new System.Drawing.Point(147, 128);
            this.txtuname.Name = "txtuname";
            this.txtuname.Size = new System.Drawing.Size(170, 22);
            this.txtuname.TabIndex = 121;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(26, 128);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 17);
            this.label2.TabIndex = 120;
            this.label2.Text = "Username";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(56, 43);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(607, 32);
            this.label1.TabIndex = 119;
            this.label1.Text = "Welcome To Passwordmanipulation System";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // password_man
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(724, 587);
            this.Controls.Add(this.txtopassword);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.txtflname);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.txtuid);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.btnexit);
            this.Controls.Add(this.btnclear);
            this.Controls.Add(this.btnsave);
            this.Controls.Add(this.lbuname);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.txtmby);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.txtcby);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.txtmdate);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.txtcdate);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.txtsuname);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.txteid);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtmono);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtlname);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtmname);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtfname);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtcpassword);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtnpassword);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtuname);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "password_man";
            this.Text = "password_man";
            this.Load += new System.EventHandler(this.password_man_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtopassword;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtflname;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtuid;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Button btnexit;
        private System.Windows.Forms.Button btnclear;
        private System.Windows.Forms.Button btnsave;
        private System.Windows.Forms.ListBox lbuname;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtmby;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtcby;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtmdate;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtcdate;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtsuname;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txteid;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtmono;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtlname;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtmname;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtfname;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtcpassword;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtnpassword;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtuname;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
    }
}